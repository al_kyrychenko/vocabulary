import React from 'react';

import styles from './HeaderBlock.module.scss';

const value = 80;
const logoStyle = {
    width: `${value}px`,
    height: `${value}px`,
}

const HeaderBlock = ({title, hideBackground = false, descr, logo, children}) => {
    const inlineStyle = hideBackground ? {backgroundImage: 'none'} : {};
    return (
        <div className={styles.cover} style={inlineStyle}>
            <div className={styles.wrap}>
                {title ? <h1 className={styles.header}>{title}</h1> : null}
                {logo ? <img style={logoStyle} src={logo} alt="React_logo"/> : null}
                <p className={styles.descr}>{descr}</p>
                {children}
            </div>
        </div>
    );
};

export default HeaderBlock;