import React from "react";
import s from './CardBlock.module.scss';
import {CheckCircleOutlined, DeleteOutlined} from '@ant-design/icons';

class Card extends React.Component {

    state = {
        done: false,
        isRemembered: false,
    }

    onClickListener() {
        if (this.state.isRemembered) return;

        this.setState((state) => {
            return {
                done: !state.done,
            }
        });
    }

    onCheck = () => {
        this.setState({
            isRemembered: true,
            done: true,
        })
    }

    onDelete = () => {
        this.props.onDeleted();
    }

    render() {
        const {rus, eng} = this.props;
        const {done, isRemembered} = this.state;

        const containerClasses = [s.card];
        const cardClasses = [s.cardInner];

        if (done) {
            containerClasses.push(s.done);
        }

        if (isRemembered) {
            cardClasses.push(s.checked);
        }

        return (
            <div className={s.container}>
                <div
                    className={containerClasses.join(' ')}
                    onClick={() => this.onClickListener()}
                >
                    <div className={cardClasses.join(' ')}>
                        <div className={s.cardFront}>
                            {eng}
                        </div>
                        <div className={s.cardBack}>
                            {rus}
                        </div>
                    </div>
                </div>
                <CheckCircleOutlined className={s.checkBox} onClick={this.onCheck}/>
                <DeleteOutlined className={s.checkBox} onClick={this.onDelete}/>
            </div>

        );
    }
}

export default Card;