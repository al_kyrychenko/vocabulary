import React from "react";
import s from './FooterBlock.module.scss';

import logo from '../../logo.svg';

const FooterBlock = ({name, color = '#025'}) => {
    return (
        <div
            className={s.copyright}
            style={{backgroundColor: color}}>
            <img src={logo} alt="" className={s.logo}/>
            <p className={s.createdWith}>Created with React.js</p>
            { name ? <small className={s.createdWith}>Copyright&copy;2020 by {name}</small> : null}
        </div>
    )
};

export default FooterBlock;