import React, { Component } from "react";
import getTranslateWord from "../../services/dictionary";
import uniqId from "uniqid";

import './Content.css';
import englishLogo from "../../english_logo.png";

import HeaderBlock from "../../components/HeaderBlock/header";
import CardBlock from "../../components/Card/card";
import FooterBlock from "../../components/FooterBlock/footer";

import FirebaseContext from "../../context/firebaseContext";
import { Input } from 'antd';
const { Search } = Input;

class HomePage extends Component{

    state = {
        wordArr: [],
        label: 'Добавляйте свои карточки со словами',
        isBusy: false,
        userUid: localStorage.getItem('user')
    }

    componentDidMount() {
        const { getUserCardsRef } = this.context;

        getUserCardsRef().on("value", res => {
            this.setState({
                wordArr: res.val() || [],
            });
            console.log('from ' + this.state.wordArr);
        })
    }

    render() {
        const {wordArr, isBusy} = this.state;

        return (
            <>
                <HeaderBlock
                    title={'Учите слова онлайн!'}
                    descr={'Воспользуйтесь карточками для запоминания и пополняйте активный словарный запас'}
                    logo={englishLogo}
                />
                <HeaderBlock
                    title={'English for everyone!'}
                    descr={'Выучи английский язык онлайн'}
                    hideBackground
                >
                    <p className={'descr'}>Зная международный язык, вы легко сможете поехать на отдых куда захотите. Вы
                        будете уверенны что вас не обманут, легко сможете общаться с местным населением
                        (узнавать про достопримечательности, узнать дорогу к месту назначения и т. д.).
                        Да, отдыхать без знания можно, есть места отдыха международного класса, где с
                        вами за n‑ную сумму денег буду говорить на вашем языке, но список таких мест
                        ограничен, вы не сможете попробовать того чего вам хотелось - вас загоняют в шаблон.
                    </p>
                </HeaderBlock>
                <div className='content_container'>
                    <div>{this.state.label}</div>
                    <Search
                        placeholder="Введите слово, которое хотите добавить"
                        enterButton="Add"
                        size="large"
                        loading={isBusy}
                        onSearch={value => this.onSubmitForm(value)}
                    />
                </div>
                <div className='content_container'>
                    {
                        wordArr.map(({eng, rus, id}) => (
                            <CardBlock onDeleted={() => this.onDeleteItem(id)} key={id} eng={eng} rus={rus}/>
                        ))
                    }
                </div>
                <FooterBlock name={'Alex Kyrychenko'}/>
            </>
        )
    }

    getWord = async (value) => {

        if (!value.trim()) {
            this.setState({
                label: 'Ошибка: пустая строка',
                isBusy: false,
            });
            return;
        }

        const wordRes = await getTranslateWord(value);

        if (wordRes.status >= 400) {
            this.setState({
                label: 'Ошибка: неизвесное слово',
                isBusy: false,
            });
            return;
        }
        const {text, translate} = wordRes;
        const {wordArr} = this.state;

        const newWordList = [...wordArr, {
            rus: translate,
            eng: text,
            id: uniqId()
        }];

        this.setState(() => {
            return {
                label: `Добавлено слово: ${value}`,
                inputValue: '',
                isBusy: false,
            }
        });

        await this.setNewWord(newWordList);
    }

    setNewWord = async (words) => {

        const { getUserCardsRef } = this.context;

        await getUserCardsRef().set(words);

        //await dataset.ref(this.urlRequest).set(words);
    }

    onSubmitForm = async (value) => {

        this.setState({
            isBusy: true,
        }, _ => this.getWord(value));

    }

    onDeleteItem(id) {
        if (!id) return;
        const { wordArr } = this.state;
        const newWordArr = wordArr.filter((item) => item.id !== id);
        this.setNewWord(newWordArr);
    }
}
HomePage.contextType = FirebaseContext;

export default HomePage;
