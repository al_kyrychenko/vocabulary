import React, { Component } from 'react';
import Login from "./pages/Login/login";
import { fire } from './services/firebase';
import HomePage from "./pages/Home/home";
import FirebaseContext from "./context/firebaseContext";

import { BrowserRouter, Route, Link, Switch, Redirect } from 'react-router-dom';
import { Layout, Menu } from "antd";
import { PrivateRoute } from "./utils/privateRoute";

const { Header, Content } = Layout;

class App extends Component{

    state = {
        user: null
    }

    componentDidMount() {

        const { auth, setUserUid } = this.context;

        auth.onAuthStateChanged((user) => {
            if (user) {
                setUserUid(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                this.setState({
                    user,
                });
            } else {
                setUserUid(null);
                localStorage.removeItem('user');
                this.setState({
                    user: false,
                })
            }
        })
    }

    render() {
        const { user } = this.state;
        if (!user) return null;
        /*return (
            <>
                { user ? <HomePage user={user}/> : <Login/> }
            </>
        )*/

        if (user) {
            return (
                <BrowserRouter>
                    <Route path={'/login'} component={Login}/>
                    <Route render={(props) => {
                        const {history: {push}} = props;
                        return (
                            <Layout>
                                <Header>
                                    <Menu theme={'dark'} mode={'horizontal'}>
                                        <Menu.Item key={'1'}>
                                            <Link to="/">Home</Link>
                                        </Menu.Item>
                                        <Menu.Item key={'2'}>
                                            <Link to="/about">About</Link>
                                        </Menu.Item>
                                        <Menu.Item key={'3'}>
                                            <Link to="/contact">Contacts</Link>
                                        </Menu.Item>
                                    </Menu>
                                </Header>
                                <Content>
                                    <Switch>
                                        <PrivateRoute path={'/'} user={user} exact component={HomePage}/>
                                        <PrivateRoute path={'/home'} user={user} component={HomePage}/>
                                        <Route path={'/about'} render={() => {
                                            return (
                                                <h1>About page</h1>
                                            );
                                        }}/>
                                        <Route path={'/contact'} render={() => {
                                            return (
                                                <h1>Contact page</h1>
                                            );
                                        }}/>
                                        <Route render={() => {
                                            return (
                                                <h1>404 page</h1>
                                            );
                                        }}/>
                                    </Switch>
                                </Content>
                            </Layout>
                        )
                    }}/>
                </BrowserRouter>
            );
        } else {
            return (
                <Login/>
            )
        }
    }
}
App.contextType = FirebaseContext;

export default App;
