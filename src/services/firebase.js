import * as firebase from "firebase/app";
import 'firebase/database';
import 'firebase/auth';

const API_KEY = process.env.REACT_APP_FIREBASE_API_KEY;
const DOMAIN = process.env.REACT_APP_FIREBASE_DOMAIN;
const URL = process.env.REACT_APP_FIREBASE_URL;
const ID = process.env.REACT_APP_FIREBASE_ID;
const BUCKET = process.env.REACT_APP_FIREBASE_BUCKET;
const SENDER_ID = process.env.REACT_APP_FIREBASE_SENDER_ID;
const APP_ID = process.env.REACT_APP_FIREBASE_APP_ID;

const firebaseConfig = {
    apiKey: API_KEY,
    authDomain: DOMAIN,
    databaseURL: URL,
    projectId: ID,
    storageBucket: BUCKET,
    messagingSenderId: SENDER_ID,
    appId: APP_ID
};

class Firebase {
    constructor() {
        firebase.initializeApp(firebaseConfig);

        this.auth = firebase.auth();
        this.database = firebase.database();
        this.userUid = null;
    }

    setUserUid = (uid) => this.userUid = uid;

    singWithEmail = (email, pass) => this.auth.signInWithEmailAndPassword(email, pass);

    getUserCardsRef = () => this.database.ref(`/${this.userUid}`)
}

export default Firebase;
