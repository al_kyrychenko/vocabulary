import React from 'react';
import ReactDom from 'react-dom';

import './style.scss';
import 'antd/dist/antd.css';

import App from "./App";
import FirebaseContext from "./context/firebaseContext";
import Firebase from "./services/firebase";

ReactDom.render(
    <FirebaseContext.Provider value={new Firebase()}>
        <App/>
    </FirebaseContext.Provider>,
    document.getElementById('app'));
