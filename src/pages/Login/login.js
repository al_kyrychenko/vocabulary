import React, { Component } from 'react';
import { Layout, Form, Input, Button } from 'antd';
import style from './login.module.scss';

import FirebaseContext from "../../context/firebaseContext";

const { Content } = Layout;

class Login extends Component {

    render() {
        return (
            <Layout>
                <Content>
                    <div className={style.root}>
                        <div className={style.form}>
                            { this.renderForm() }
                        </div>
                    </div>
                </Content>
            </Layout>
        )
    }

    renderForm = () => {

        const layout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
        };

        const tailLayout = {
            wrapperCol: { offset: 8, span: 16 },
        };

        return (
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Username"
                    name="email"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="pass"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        )
    }

    onFinish = values => {
        const { email, pass } = values;
        const { singWithEmail, setUserUid } = this.context;
        const { history } = this.props;

        singWithEmail(email, pass)
            .then(res => {
                setUserUid(res.user.uid);
                localStorage.setItem('user', res.user.uid);
                history.push('/');
            });
    };

    onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };
}
Login.contextType = FirebaseContext;

export default Login;
